module Protocol
  ( headerV1
  ) where


import GHC.Generics (Generic)
import Data.Aeson
  ( ToJSON(..)
  , genericToJSON
  , defaultOptions
  , Options(omitNothingFields)
  )


-- DOMAIN


-- | The header is a JSON object with support for the following properties (only version is required).
data Header = Header
  { version :: Integer -- ^ The protocol version to use. Currently, this must be 1
  , clickEvents :: Maybe Bool -- ^ Whether to receive click event information to standard input
  , contSignal :: Maybe Integer -- ^ The signal that swaybar should send to continue processing
  , stopSignal :: Maybe Integer -- ^ The signal that swaybar should send to stop processing
  }
  deriving (Generic)


-- CONSTANTS


headerV1 :: Header
headerV1 = Header
  { version = 1
  , clickEvents = Nothing
  , contSignal = Nothing
  , stopSignal = Nothing
  }


-- INSTANCES


instance ToJSON Header where
  toJSON = genericToJSON defaultOptions { omitNothingFields = True }
