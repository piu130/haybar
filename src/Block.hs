module Block
  ( Name(..)
  , Instance_
  , Id(..)
  ) where


import Data.Aeson
  ( FromJSON(..)
  , FromJSONKey(..)
  , FromJSONKeyFunction(FromJSONKeyValue)
  , withText
  , Value(String)
  )
import Data.Text (Text, splitOn)
import Prelude hiding (Left, Right)


-- DOMAIN


data Name
  = Brightness
  | Clock
  deriving (Eq, Show)


newtype Instance_ = Instance_ Text
  deriving (Eq, Show)


data Id = Id
  { name :: Name
  , instance_ :: Instance_
  }
  deriving (Eq, Show)


-- CONSTANTS


-- INSTANCES


instance FromJSON Name where
  parseJSON (String "brightness") = pure Brightness
  parseJSON (String "clock") = pure Clock
  parseJSON _ = fail "Expect one of [brightness, clock]."


instance FromJSON Instance_ where
  parseJSON (String s) = pure (Instance_ s)
  parseJSON _ = fail "Expect string"


instance FromJSON Id where
  parseJSON = withText "block id" $ \t ->
    let split = splitOn "." t in
    Id <$>
    parseJSON (String (head split)) <*>
    parseJSON (String (last split))


instance FromJSONKey Id where
  fromJSONKey = FromJSONKeyValue parseJSON
