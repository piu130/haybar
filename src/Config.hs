module Config
  ( Config
  , BlockOrder
  ) where


import qualified Block
import qualified Block.Defaults as BD
import qualified Block.Brightness as BB
import qualified Block.Clock as BC

import Data.Traversable (for)
import Data.Aeson
  ( FromJSON(..)
  , withObject
  , withArray
  , (.:)
  , (.:?)
  , (.!=)
  , Value(Array, String)
  )
import qualified Data.HashMap.Strict as HM
import Data.Aeson.Types (Parser)
import Data.Text (Text)


-- DOMAIN


newtype BlockOrder = BlockOrder [Block.Id]
  deriving (Show)


data Block
  = BlockBrightness Block.Instance_ BB.Brightness
  | BlockClock Block.Instance_ BC.Clock
  deriving (Eq, Show)


newtype Blocks = Blocks [Block]
  deriving (Show)


data Config = Config
  { defaults :: BD.Defaults
  , blockOrder :: BlockOrder
  , blocks :: Blocks
  }
  deriving (Show)


-- CONSTANTS


-- INSTANCES


instance FromJSON BlockOrder where
  parseJSON = withArray "block order" $ \a ->
    BlockOrder <$> parseJSON (Array a)


instance FromJSON Blocks where
  parseJSON = withObject "blocks" $ \o ->
    Blocks <$> for (HM.toList o) (\(key, value) ->
      do blockId <- parseJSON (String key)
         parseBlock blockId value)

    where parseBlock blockId block =
            case Block.name blockId of
              Block.Brightness ->
                BlockBrightness
                (Block.instance_ blockId) <$>
                (parseJSON block <*> pure BD.defaults)
              Block.Clock ->
                BlockClock
                (Block.instance_ blockId) <$>
                (parseJSON block <*> pure BD.defaults)



-- TODO check if brightness etc.
parseBlock2 :: (Text, Value) -> Parser (BD.Defaults -> Block)
parseBlock2 (key, value) =
  do blockId <- parseJSON (String key) :: Parser Block.Id
     block <- parseJSON value :: Parser (BD.Defaults -> BB.Brightness)
     case Block.name blockId of
          Block.Brightness -> pure $ \def -> BlockBrightness (Block.instance_ blockId) (block def)
        --  Block.Clock -> pure $ \def -> Clock (Block.instance_ blockId) (block def)
          -- TODO remove _ -> fail ...
    --  pure $ \def ->
    --    case Block.name blockId of
    --         Block.Brightness -> Brightness (Block.instance_ blockId) (block (def :: BlockDefaults))
    --         _ -> fail "String"
    --  Brightness <$>
    --  parseJSON (String key) <*>
    --  (parseJSON value <*> pure def)


-- instance FromJSON (BlockDefaults -> Blocks) where
--   parseJSON = withObject "defaults -> blocks" $ \o ->
--     pure $ \def ->
--       Blocks <$> (for (HM.toList o) $ \kv -> parseBlock def kv)

-- -- TODO fix and implement
-- instance FromJSON (BlockDefaults -> Blocks) where
--   parseJSON = withObject "defaults -> blocks" $ \o ->
--     pure $ \def ->
--       Blocks <$> (for (HM.toList o) $ \kv -> (parseBlock2 kv) <$> def)


instance FromJSON Config where
  parseJSON = withObject "config" $ \o ->
    let def = o .: "defaults" in
    Config <$>
    def <*>
    o .:? "block_order" .!= BlockOrder [] <*>
    -- TODO use BlockDefaults -> Block parser and pass def
    o .: "blocks"
