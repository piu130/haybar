module Block.Clock
  ( Clock
  ) where


import Block.Defaults (Defaults, defaults)
import Data.Text (Text)
import Data.Aeson
  ( FromJSON(..)
  , withObject
  , (.:?)
  )
import Data.Maybe (fromMaybe)


-- DOMAIN


data Clock = Clock
  { defaultValues :: Defaults
  , format :: Text
  , intervall :: Integer
  }
  deriving (Eq, Show)


-- CONSTANTS


defClock :: Clock
defClock = Clock
  { defaultValues = defaults
  , format = "bright format{{"
  , intervall = 30
  }


-- INSTANCES


instance FromJSON (Defaults -> Clock) where
  parseJSON val = (withObject "defaults -> clock" $ \o ->
    do f <- o .:? "format"
       i <- o .:? "intervall"
       d <- parseJSON val
       pure $ \def ->
         Clock
           (d def)
           (fromMaybe (format defClock) f)
           (fromMaybe (intervall defClock) i)
         )
         val

