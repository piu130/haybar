module Block.Brightness
  ( Brightness
  ) where


import Block.Defaults (Defaults, defaults)
import Data.Text (Text)
import Data.Aeson
  ( FromJSON(..)
  , withObject
  , (.:?)
  )
import Data.Maybe (fromMaybe)


-- DOMAIN


data Brightness = Brightness
  { defaultValues :: Defaults
  , format :: Text
  }
  deriving (Eq, Show)


-- CONSTANTS


defBrightness :: Brightness
defBrightness = Brightness
  { defaultValues = defaults
  , format = "bright format{{"
  }


-- INSTANCES


instance FromJSON (Defaults -> Brightness) where
  parseJSON val = (withObject "defaults -> brightness" $ \o ->
    do f <- o .:? "format"
       d <- parseJSON val
       pure $ \def ->
         Brightness
           (d def)
           (fromMaybe (format defBrightness) f)
         )
         val
