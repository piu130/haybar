module Block.Defaults
  ( Defaults(..)
  , defaults
  , Align
  ) where


import Data.Text (Text)
import Data.Aeson
  ( FromJSON(..)
  , withObject
  , (.:?)
  , Value(String)
  )
import Prelude hiding (Left, Right)
import Data.Maybe (fromMaybe)


-- DOMAIN


data Align
  = Left
  | Center
  | Right
  deriving (Eq, Show)


data Defaults = Defaults
  { align :: Align
  -- TODO use colour package?
  , background :: Text
  }
  deriving (Eq, Show)


-- CONSTANTS


defaults :: Defaults
defaults = Defaults
  { align = Center
  , background = "red"
  }


-- INSTANCE


instance FromJSON Align where
  parseJSON (String "left")   = pure Left
  parseJSON (String "center") = pure Center
  parseJSON (String "right")  = pure Right
  parseJSON _ = fail "Expect one of [left, center, right]."


instance FromJSON Defaults where
  parseJSON val = parseJSON val <*> pure defaults


instance FromJSON (Defaults -> Defaults) where
  parseJSON = withObject "defaults -> defaults" $ \o ->
    do a <- o .:? "align"
       b <- o .:? "background"
       pure $ \def ->
         Defaults
         (fromMaybe (align def) a)
         (fromMaybe (background def) b)
