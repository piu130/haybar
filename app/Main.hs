module Main where

import Data.Aeson (encode)
import qualified Data.Yaml as Y
import qualified Data.ByteString as BI
import Data.ByteString.Lazy.Char8 (putStrLn)
import Config (Config)
-- import System.INotify (initINotify, addWatch, removeWatch, EventVariety(CloseWrite))
import Protocol (headerV1)
-- import Control.Concurrent.Timer (repeatedTimer)
-- import Control.Concurrent.Suspend.Lifted (sDelay)
import Prelude hiding (putStrLn)


data Event
  = Time
  | FileChanged
  | Click
  | Scroll


main :: IO ()
main = do
  configY <- BI.readFile "config/config.yaml"

  let eitherConfig = Y.decodeEither' configY :: Either Y.ParseException Config
  case eitherConfig of
    Left e -> print e
    Right config -> print config

  putStrLn $ encode headerV1
  putStrLn "["

  -- _ <- repeatedTimer (putStrLn "1") (sDelay 2)

  print (1 :: Integer)


  -- inotify <- initINotify
  -- print inotify

  -- wd <- addWatch
  --   inotify
  --   [CloseWrite]
  --   "/sys/class/backlight/intel_backlight/brightness"
  --   print

  -- print wd
  -- _ <- getLine
  -- removeWatch wd
